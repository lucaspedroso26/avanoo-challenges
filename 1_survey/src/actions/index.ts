import { EDIT_QUESTION, SET_LIST_QUESTIONS } from './actionTypes'

export const actionSetListQuestions = (listQuestions: Array<any>) => ({
    type: SET_LIST_QUESTIONS,
    payload: listQuestions
})

export const actionSetEditQuestion = (question: any) => ({
    type: EDIT_QUESTION,
    payload: question
})

export default {
    actionSetListQuestions,
    actionSetEditQuestion
}