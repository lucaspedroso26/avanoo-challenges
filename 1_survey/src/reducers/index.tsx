import { combineReducers } from 'redux'
import { EDIT_QUESTION, SET_LIST_QUESTIONS } from '../actions/actionTypes'
import { QuestionProps } from '../dataTypes/types'
type TactionProps = {
    type: string,
    payload: any
}

interface IEditQuestionReducer {
    (initialData: any, actionProps: TactionProps): any
}
const editingItemInitialData: QuestionProps = {
    label: '',
    type: 'Text',
    options: [{ value: '', isAnswer: false }],
    answer: null
}
const localStorageQuestions = localStorage.getItem('list_questions')
const listQuestions:Array<QuestionProps> = localStorageQuestions !== null ? JSON.parse(localStorageQuestions) : []

export const listQuestionsReducer: IEditQuestionReducer = (initialData = listQuestions, { type, payload }) => (
    type === SET_LIST_QUESTIONS ? [...payload] : initialData
)

export const editQuestionReducer: IEditQuestionReducer = (initialData = editingItemInitialData, { type, payload }) => (
    type === EDIT_QUESTION ? { ...initialData, ...payload } : initialData
)

export default combineReducers({
    listQuestions: listQuestionsReducer,
    editQuestion: editQuestionReducer,
})