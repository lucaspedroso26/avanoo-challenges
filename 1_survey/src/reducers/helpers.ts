import { QuestionProps } from "../dataTypes/types"
import { actionSetListQuestions } from "../actions"

export const mapStateToProps = ({ listQuestions }: any) => ({ listQuestions })
export const mapDispatchToProps = (dispatch: any) => ({
    setList: (list: Array<QuestionProps>) => dispatch(actionSetListQuestions(list))
})