export type EditingTypeOption = {
    value: string,
    label: string
}

export type OptionType = {
    value: any,
    isAnswer: boolean,
    isChecked?: boolean
}

export type QuestionProps = {
    label: string,
    options: Array<OptionType>,
    type: string,
    answer: any
}