import React from 'react'
import Menu from '../components/Menu'
import {
    HashRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom'
import EditSurvey from '../pages/EditSurvey'
import Survey from '../pages/Survey'
import Setup from '../pages/Setup'
const Navigation = () => {
    return (
        <>
            <div className="app-navigation container">
                <Router>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <Link to='survey' className="nav-item nav-link" href="#">Survey</Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                                <Link to="/" className="nav-item nav-link active" href="#">Setup</Link>
                                <Link to="/edit" className="nav-item nav-link" href="#">Edit</Link>
                                <Link to='survey' className="nav-item nav-link" href="#">Survey</Link>
                            </div>
                        </div>
                    </nav>
                    <Switch>
                        <Route path="/" exact>
                            <Setup />
                        </Route>
                        <Route path="/survey">
                            <Survey />
                        </Route>
                        <Route path="/edit">
                            <EditSurvey />
                        </Route>
                    </Switch>
                </Router>
            </div>
        </>
    )
}

export default Navigation
