import React from 'react'
import './index.scss'
import * as OptionsComponents from '../../configOptions'
import QuestionEditActions from '../QuestionEditActions'
const OptionsComponentsTags: any = OptionsComponents

interface IProperty {
    type: string
}

type QuestionProps = {
    label: string,
    properties: IProperty,
    id?: any,
    isEdit?: boolean
}

const Question: any = ({ label, properties, id, isEdit = false }: QuestionProps) => {
    let Option = null
    if (typeof OptionsComponentsTags[properties.type] !== 'undefined') {
        Option = OptionsComponentsTags[properties.type]
    }

    return (
        <div className="question-wrapper card">
            {isEdit && <QuestionEditActions id={id} />}
            <h4>{label}</h4>
            {Option && <Option id={id} {...properties} />}
        </div>
    )
}

export default Question
