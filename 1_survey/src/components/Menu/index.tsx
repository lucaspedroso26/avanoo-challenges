import React from 'react'
import { Link } from 'react-router-dom'
import './index.scss'

const Menu = () => {
    return (
        <div className="navbar-nav">
            <Link to="/" className="nav-item nav-link active" href="#">Setup</Link>
            <Link to="/edit" className="nav-item nav-link" href="#">Edit</Link>
            <Link to='survey' className="nav-item nav-link" href="#">Survey</Link>
        </div>
    )
}

export default Menu
