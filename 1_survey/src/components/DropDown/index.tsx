import React from 'react'
import { connect } from 'react-redux'
import { mapDispatchToProps, mapStateToProps } from '../../reducers/helpers'
const Dropdown = (props: any) => {
    const handleChange = (event: any) => {
        event.preventDefault()
        if (props.id) {
            let { listQuestions } = props;
            let { value } = event.target;
            let current = listQuestions[props.id];
            current.options = current.options.map((item: any) => ({ ...item, isChecked: false }))
            current.options[value].isChecked = !current.options[value].isChecked;
            listQuestions[props.id] = current;
            props.setList(listQuestions);
        }
    }
    return (
        <div className="form-group">
            <select className="form-control" onChange={handleChange}>
                <option></option>
                {props.options.map((item: any, key: number) => <option key={key} value={key} selected={item.isChecked}> {item.value} </option>)}
            </select>
        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Dropdown)
