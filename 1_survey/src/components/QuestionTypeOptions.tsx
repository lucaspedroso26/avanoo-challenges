import React from 'react'

type QuestionTypeOptionsProps = {
    options: any,
    handler: any,
    current: string
}

const QuestionTypeOptions = ({ options, handler, current }: QuestionTypeOptionsProps) => (
    <div className="form-group-wrapper">
            <div className="btn-group" role="group">
            {options.map((item:any,key: number) => (
                <button
                key={key}
                type="button"
                value={item.value}
                name="type"
                onClick={handler}
                className={`btn btn-secondary ${current === item.value ? 'active' : null}`} >
                {item.label}
                </button>
            ))}
            </div>
        </div>
)

export default QuestionTypeOptions
