import React from 'react'
import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from '../../reducers/helpers'

const Multicheckbox = (props: any) => {

    const handleChange = (event: any, key: number) => {
        event.preventDefault()
        if (props.id) {
            let { listQuestions } = props;
            let current = listQuestions[props.id];
            current.options[key].isChecked = !current.options[key].isChecked;
            listQuestions[props.id] = current;
            props.setList(listQuestions);
        }
    }

    return (
        <div>
            {props.options.map((item: any, key: number) => (
                item.value !== '' && <div key={key} className="multicheckbox-item">
                    <div className="form-group form-check">
                        <input name="isAnswer" type="checkbox" className="form-check-input" checked={item.isChecked} onChange={e => handleChange(e, key)} />
                        <label className="form-check-label">{item.value}</label>
                    </div>
                </div>
            ))}
        </div>
    )
}


export default connect(mapStateToProps, mapDispatchToProps)(Multicheckbox)
