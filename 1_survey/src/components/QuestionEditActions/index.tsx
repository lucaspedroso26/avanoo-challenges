import React from 'react'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { actionSetEditQuestion, actionSetListQuestions } from '../../actions'
import './index.scss'
import { QuestionProps } from '../../dataTypes/types'
type QuestionEditActionsProps = {
    id?: any
    listQuestions?: any,
    setEdit?: any,
    setList?: any,
}
const QuestionEditActions = ({ id, listQuestions, setEdit, setList }: QuestionEditActionsProps) => {
    const history:any = useHistory()
    const deleteAction = (event: any) => {
        event.preventDefault()
        delete listQuestions[id]
        listQuestions = listQuestions.filter((item: any) => item !== undefined)
        setList(listQuestions);
    }
    const editAction = (event: any) => {
        event.preventDefault()
        listQuestions[id].id = id;
        setEdit(listQuestions[id])
        history.push('/')
    }
    return (
        <div className="btn-group btn-actions-wrapper">
            <button onClick={editAction} className="btn btn-primary">Edit</button>
            <button onClick={deleteAction} className="btn btn-danger">Delete</button>
        </div>
    )
}
const mapStateToProps = ({ listQuestions }: any) => ({ listQuestions })

const mapDispatchToProps = (dispatch: any) => ({
    setEdit: (data: QuestionProps) => dispatch(actionSetEditQuestion(data)),
    setList: (data: Array<QuestionProps>) => dispatch(actionSetListQuestions(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(QuestionEditActions)
