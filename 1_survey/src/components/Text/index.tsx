import React from 'react'
import { connect } from 'react-redux'
import {
    mapStateToProps,
    mapDispatchToProps
} from '../../reducers/helpers'

const Text = (props: any) => {
    const handleChange = (event: any) => {
        event.preventDefault();
        if (props.id !== undefined || props.id !== null) {
            let { listQuestions } = props;
            debugger;
            listQuestions[props.id].answer = event.target.value;
            props.setList(listQuestions)
        }
        return false
    }
    let answer =  props.listQuestions[props.id].answer || props.answer
    return (
        <div className="form-group">
            <input type="text" className="form-control" onChange={handleChange} value={answer} />
        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Text)
