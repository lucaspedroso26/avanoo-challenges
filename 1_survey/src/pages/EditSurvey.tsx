import React from 'react'
import { connect } from 'react-redux'
import { QuestionProps } from '../dataTypes/types'
import Question from '../components/Question'

type SurveyProps = {
    listQuestions: Array<QuestionProps>
}

const Survey = ({ listQuestions }: SurveyProps) => {
    return (
        <div className="page survey">
            {listQuestions.length > 0 && listQuestions.map((item, key) => <Question isEdit={true} id={key} label={item.label} properties={item} />) }
        </div>
    )
}

const mapStateToProps = ({ listQuestions }: SurveyProps) => ({ listQuestions })

export default connect(mapStateToProps)(Survey)
