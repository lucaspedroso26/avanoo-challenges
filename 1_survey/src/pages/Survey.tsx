import React, { useState } from 'react'
import { connect } from 'react-redux'
import { QuestionProps } from '../dataTypes/types'
import Question from '../components/Question'

type SurveyProps = {
    listQuestions: Array<QuestionProps>
}

const Survey = ({ listQuestions }: SurveyProps) => {
    let [current, setCurrent] = useState(0)
    let [checkAll, setCheckAll] = useState(false)
    const nextIsDisabled = () => {
        if (listQuestions[current].type !== 'Text') {
            let isFilled = listQuestions[current].options.filter((item: any) => item.isChecked).length > 0;
            return !isFilled && !(current > listQuestions.length - 1)
        }
        else {
            return (!listQuestions[current].answer && !(current > listQuestions.length - 1))
        }
    }
    const previousIsDisabled = () => !(current > 0)
    const nextQuestion = (event: any) => {
        event.preventDefault()
        if (current < listQuestions.length) {
            setCurrent(current + 1)
        }
    }
    const previousQuestion = () => {
        if (current > 0) {
            setCurrent(current - 1)
        }
    }
    if(listQuestions.length > 0){

        return (
            <div className="page survey">
                {!checkAll &&
                    <>
                        <Question id={current} label={listQuestions[current].label} properties={listQuestions[current]} />
                        <div className="row">
                            <div className="col-md-6">
                                <button onClick={previousQuestion} disabled={previousIsDisabled()} className="btn btn-primary btn-block">Previous</button>
                            </div>
                            <div className="col-md-6">
                                <button onClick={nextQuestion} disabled={nextIsDisabled()} className="btn btn-primary btn-block">Next</button>
                            </div>
                            {(!(current < listQuestions.length - 1)) &&
                                <div className="col-md-12 submit-row">
                                    <button className="btn btn-dark btn-block" onClick={() => {
                                        setCheckAll(!checkAll)
                                    }}>
                                        Submit
                                    </button>
                                </div>
                            }
    
                        </div>
                    </>}
    
                {checkAll && <div className="results">
                    {listQuestions.map((item, key) => <Question isEdit={true} id={key} label={item.label} properties={item} />)}
                    Thank you for answer the questions!
                </div>}
            </div>
        )
    }
    else{
        return null
    }
}

const mapStateToProps = ({ listQuestions }: SurveyProps) => ({ listQuestions })

export default connect(mapStateToProps)(Survey)
