import React, { Component } from 'react'
import Question from '../components/Question'
import QuestionTypeOptions from '../components/QuestionTypeOptions'
import { EditingTypeOption, QuestionProps, OptionType } from '../dataTypes/types'
import { connect } from 'react-redux'
import { actionSetEditQuestion, actionSetListQuestions } from '../actions'
const options: Array<EditingTypeOption> = [
    {
        value: 'Multicheckbox',
        label: 'Multicheckbox'
    },
    {
        value: 'Text',
        label: 'Text'
    },
    {
        value: 'Radio',
        label: 'Radio'
    },
    {
        value: 'Dropdown',
        label: 'Dropdown'
    }
]
type SetupProps = {
    editQuestion: any,
    listQuestions: Array<QuestionProps>,
    setEdit: any
    setList: any
}
type SetupState = {
    editingQuestionKey: any,
    editingQuestion: QuestionProps,
    message: string,
}

export class Setup extends Component<SetupProps, SetupState> {
    state = {
        editingQuestionKey: null,
        editingQuestion: this.props.editQuestion,
        message: ''
    }
    handleChangeEditingQuestion = (event: any) => {
        let { name, value } = event.target;
        let editingQuestion = this.radioUnCheckAll(value === 'Radio')
        let item = { [name]: value }
        editingQuestion = { ...editingQuestion, ...item }
        this.setState({
            editingQuestion
        })
    }
    radioUnCheckAll = (isRadio?: boolean) => {
        let { editingQuestion } = this.state;
        if (editingQuestion.type !== 'Multicheckbox' || isRadio) {
            editingQuestion.options = editingQuestion.options.map((item: any) => {
                item.isAnswer = false
                return item;
            })
        }
        return editingQuestion;
    }
    handleOptionItem = (event: any, key: number) => {
        event.preventDefault()
        let editingQuestion = this.radioUnCheckAll()
        let { value, name } = event.target;
        if (name === 'isAnswer') {
            value = !editingQuestion.options[key].isAnswer
        }
        let item = { [name]: value }
        editingQuestion.options[key] = { ...editingQuestion.options[key], ...item };
        this.setState({
            editingQuestion
        })
        return false;
    }

    addOption = (event: any) => {
        event.preventDefault()
        let { editingQuestion } = this.state;
        editingQuestion.options.push({ value: '', isAnswer: false })
        this.setState({
            editingQuestion
        })
    }
    removeOption = (event: any, key: number) => {
        event.preventDefault()
        let { editingQuestion } = this.state;
        delete editingQuestion.options[key]
        this.setState({
            editingQuestion
        })
    }
    enableSave = () => {
        let { editingQuestion } = this.state;
        if (editingQuestion.type !== 'Text') {
            let validOptions = editingQuestion.options.filter((item: OptionType) => item.value !== '')
            return editingQuestion.label !== '' && validOptions.length
        }
        else {
            return editingQuestion.label !== '';
        }
    }
    saveQuestion = (event: any) => {
        event.preventDefault()
        let { listQuestions } = this.props;
        if (this.props.editQuestion.id) {
            listQuestions[this.props.editQuestion.id] = this.state.editingQuestion
        }
        else {
            listQuestions.push(this.state.editingQuestion)
        }
        this.props.setList(listQuestions);
        localStorage.setItem('list_questions', JSON.stringify(listQuestions))
        this.setState({
            message: 'Question Saved'
        })

        setTimeout(() => {
            this.setState({
                message: '',
                editingQuestion: {
                    label: '',
                    options: [{ isAnswer: false, value: '' }],
                    type: 'Text',
                    answer: ''
                }
            })
        }, 2000);
        return false;
    }
    render() {
        let { editingQuestion } = this.state;
        return (
            <div className="page setup row">
                <div className="col-md-6">
                    <form className="add-question" onSubmit={this.saveQuestion}>
                        <div className="form-group">
                            <h1>Question:</h1>
                            <input onChange={this.handleChangeEditingQuestion} className="form-control" type="text" name='label' value={editingQuestion.label} />
                        </div>
                        <div className="form-group">
                            <label>Type of answer:</label>
                            <QuestionTypeOptions
                                options={options}
                                handler={this.handleChangeEditingQuestion}
                                current={editingQuestion.type} />
                        </div>
                        <div className="form-group">
                            <div className="adding-options row">
                                {editingQuestion.type !== 'Text' && editingQuestion.options.map((item: OptionType, key: number) => (
                                    <div key={key} className="option-item col-md-12">
                                        <input key={key} type="text" name="value" onChange={e => this.handleOptionItem(e, key)} className="form-control" value={item.value} style={{ width: 200 }} />
                                        <div className="btn-group">
                                            <button className="btn btn-primary" onClick={this.addOption}>+</button>
                                            <button className="btn btn-danger" onClick={event => this.removeOption(event, key)}>-</button>
                                        </div>
                                        <div>
                                            <div className="form-group form-check">
                                                <input onChange={e => this.handleOptionItem(e, key)} name="isAnswer" type="checkbox" className="form-check-input" checked={item.isAnswer ? true : false} />
                                                <label className="form-check-label">Is answer</label>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <button type="submit" disabled={!this.enableSave()} className="btn btn-primary">Save</button>
                        <div className="message">{this.state.message}</div>
                    </form>
                </div>
                <div className="col-md-6">
                    <div className="preview-question">
                        <h1>Preview:</h1>
                        <Question id={null} label={editingQuestion.label} properties={editingQuestion} />
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = ({ editQuestion, listQuestions }: any) => ({ editQuestion, listQuestions })

const mapDispatchToProps = (dispatch: any) => ({
    setEdit: (data: any) => dispatch(actionSetEditQuestion(data)),
    setList: (data: any) => dispatch(actionSetListQuestions(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Setup)